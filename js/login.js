(
  function() {

    $(document).ready(function(){
      var URI = config.URL + ":" + config.PORT + "/";

      /**
       *  EVENTOS
       **/
      $('#loginButton').on('click', function() {
        var valUser = $('#user').val();
        var valPass = $('#pass').val();
        
        var url = URI + "login";

        // Funcion ajax para realizar la llamada al servicio
        getCallJSONP(url, valUser, valPass).done(
          successGetCallJSONP
        ).fail(
          failGetCallJSONP
        );

        return false; // keeps the page from not refreshing
      });

      // evento para cerrar la alerta
      $('#alertWindow .close').click(function() {
        $('#alertWindow').hide(); // usamos este evento ya que la ejecucion normal (data-dismiss="alert") no permite de nuevo mostrar la alerta
      })

    });

    var getCallJSONP = function(url, user, pass){
      $('#loadingBar').show();
      $("#alertWindow").hide();
      return $.ajax({
          url: url,
          contentType: "application/json; charset=utf-8",
          type: "GET",
          data: { user: user, pass: pass, apikey: config.API_KEY },
          dataType: "jsonp" // You can't POST using JSONP...it simply doesn't work that way, it creates a <script> element to fetch data...which has to be a GET request.
      });
    };

    var successGetCallJSONP = function(data){
      $('#loadingBar').hide();
      if (data === "true") {
        window.location.replace("index.html");
      } else {
        showError();
      }
    };

    var failGetCallJSONP = function(responseData, textStatus, errorThrown) {
      $('#loadingBar').hide();
      showError(true);
    }

    // cuando no existen elementos a mostrar
    var showError = function(error) {
      var _text = "<strong>Advertencia!</strong> Fallo en la autenticación.";
      var classWarning = "alert-warning";
      var classDanger = "alert-danger";
      var currentClass = classWarning;
      $('#alertWindow').removeClass( classWarning + " " + classDanger );
      if (error) {
        _text = "<strong>Error!</strong> Error en el servidor.";
        currentClass = classDanger;
      }

      $("#alertWindow").contents().filter( // borramos el contenido del alert nodeType 3 = Text
        function(){
          return this.nodeType == 3;
        }).remove();
      $('#alertWindow *').slice(1).remove(); // borramos todos los elementos menos el primero
      $('#alertWindow').addClass(currentClass);
      $('#alertWindow').append(
        _text
      );
      $("#alertWindow").show();
    }

  }
)();
