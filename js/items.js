(
  function() {

    // comprobamos que estamos logeados
    var checkSession = function() {
      if (!$.cookie('sessionid')) {
        window.location.replace("login.html");
        $('body').css('display', 'none');
      } else {
        $('body').css('display', 'block');
      }
    }

    checkSession();

    var currentItems = [];
    
    $(document).ready(function(){
      var URI = config.URL + ":" + config.PORT + "/";

      /**
       *  EVENTOS
       **/
      $('#searchButton').on('click', function() {
        var valSearch = $('#searchInput').val();
        
        if (valSearch.split(" ").join("") !== "") {
          var url = URI + "item/" + valSearch;

          // Funcion ajax para realizar la llamada al servicio
          getCallJSONP(url).done(
            successGetCallJSONP
          ).fail(
            failGetCallJSONP
          ); 
        }

        return false; // keeps the page from not refreshing
      });

      $('a').on('click', function() {
        var action = $(this).attr('href');

        var urlData = URI + "itemsData";
        var urlModel = URI + "itemsModel";
        var urlLogout = URI + "logout"
        var url = urlData;

        var isActionServer = (action === "data" || action === "modelo" || action === "logout");
        var sucessFunction = successGetCallJSONP;
        if (action == "data") {
          url = urlData;
        } else if (action === "modelo") {
          url = urlModel;
        } else if (action === "logout") {
          url = urlLogout;
          sucessFunction = successGetLogoutCallJSONP;
        }
        
        if (isActionServer) {
          // Funcion ajax para realizar la llamada al servicio
          getCallJSONP(url).done(
            sucessFunction
          ).fail(
            failGetCallJSONP
          );
        } else {
          if (action === "cleanAll") {
            $('#myContent *').remove(); // delete children
            $('ul#items a').remove(); // delete a elements in li
            $('ul#items li:not(:first)').remove(); // delete children not first
            $('#searchInput').val(''); // borramos el contenido del input de busqueda
          } else if (action === "clean") {
            $('#myContent *').remove(); // delete children         
          }
          $('#mainCanvas').show();
        }

        return false; // keeps the page from not refreshing 

      });

      /**
      * event delegation
      * http://net.tutsplus.com/tutorials/javascript-ajax/quick-tip-javascript-event-delegation-in-4-minutes/
      *
      * (...)
      * Imagine that you have five hundred anchor tags on your page. As you might imagine, adding a click
      * event to every one of those would be time consuming, and unnecessary. On top of that, what happens
      * if, once you click on those anchor tags, we add additional anchor elements to the page? Would those
      * new anchors be bound to the click event as well? The answer is no. You would then have to reattach 
      * a listener to those newly created elements.
      * (...)
      **/
      $('#items').on('click', 'li.elementos a', function(ev) {
        var _id = ev.target.id;

        var elem = findElement(currentItems, "id", _id);

        var existsInMyContent = ( $('#myContent').find('#'+_id).length > 0 );
        if (elem && !existsInMyContent) {
          $('#mainCanvas').hide();
          $('#alertWindow').hide();
          loadDataInMyContent(elem);
        }

        return false; // keeps the page from not refreshing
      });

      // event delegation
      $('#myContent').on('click', 'p button.btnTag', function(ev) {
        var content = ev.target.innerHTML;

        if (content !== "") {
          var url = URI + "tag/" + content;

          // Funcion ajax para realizar la llamada al servicio
          getCallJSONP(url).done(
            successGetTagCallJSONP
          ).fail(
            failGetCallJSONP
          ); 
        }

        return false; // keeps the page from not refreshing
      })

      // evento para cerrar la alerta
      $('#alertWindow .close').click(function() {
        $('#alertWindow').hide(); // usamos este evento ya que la ejecucion normal (data-dismiss="alert") no permite de nuevo mostrar la alerta
      })

    });

    var getCallJSONP = function(url,data){
      $('#loadingBar').show();
      $("#alertWindow").hide();
      return $.ajax({
          //url: "http://api.flickr.com/services/feeds/photos_public.gne?tags=cat&tagmode=any&format=json&jsoncallback=?",
          url: url,
          contentType: "application/json; charset=utf-8",
          type: "GET",
          data: { apikey: config.API_KEY },
          dataType: "jsonp"
      });
    };

    // funcion que se llama si se ha pulsado en logout
    var successGetLogoutCallJSONP = function(data) {
      $('#loadingBar').hide();
      checkSession();
    }

    var successGetCallJSONP = function(data){
      $('#loadingBar').hide();
      data = $.parseJSON(data);
      if (data && data.length > 0) {
        $('#mainCanvas').show(); // show mainCanvas
        $('#myContent *').remove(); // delete children
        $('ul#items a').remove(); // delete a elements in li
        $('ul#items li:not(:first)').remove(); // delete children not first
        currentItems = []; // limpiamos el array de items
      } else {
        noDataFound();
      }
      $.each(data, function(index, element) {
        currentItems.push(element); // cargamos los elementos en el array temporal para obtener su valor cuando se pinche en un item
        loadDataInItems(element);
      });
    };

    var successGetTagCallJSONP = function(data) {
      $('#loadingBar').hide();
      data = $.parseJSON(data);
      if (data && data.length > 0) {
        $('#myContent *').remove(); // delete children
      } else {
        noDataFound();
      }
      $('#mainCanvas').hide(); // hide mainCanvas
      $.each(data, function(index, element) {
        loadDataInMyContent(element);
      });
    }

    var failGetCallJSONP = function(responseData, textStatus, errorThrown) {
      $('#loadingBar').hide();
      noDataFound(true);
    }

    // cargamos los datos en el listado de id = items
    var loadDataInItems = function(element) {
      element.id = getElementId(element);
      $('#items').append($('<li>', {
        class: "elementos",
        html: '<a id="' + element.id + '" href="#">' + element.title + '</a>'
      }));
    }

    // obtenemos el id del item
    var getElementId = function(element) {
      // dependiendo si los elementos vienen del modelo o de los datos
      var _id = (element.id && element.id !== "" ? element.id : (element._id && element._id._inc && element._id._inc !== "" ? element._id._inc : "") );
      return _id + "";
    }

    // cargamos los datos en el listado de id = myContent
    var loadDataInMyContent = function(data) {
      data.id = getElementId(data);
      
      var htmlTags = "";
      $.each(data.tags, function(index, tag) {
        var typeButton = "btn-primary";
        if (/a/i.test(tag)) typeButton = "btn-success";
        if (/b/i.test(tag)) typeButton = "btn-warning";
        if (/c/i.test(tag)) typeButton = "btn-danger";
        htmlTags += "<button type='button' class='btn " + typeButton + " btnTag'>" + tag + "</button> ";
      })
      
      $('#myContent').append($('<h2>', {
        id: data.id,
        html: data.title
      })).append($('<p>', {
        html: data.body
      })).append($('<p>', {
        html: htmlTags
      }));
    }

    // obtenemos un elemento de un array
    var findElement = function(array, propName, propValue) {
      for (var i=0; i < array.length; i++) {
        if (array[i][propName] === propValue) {
          return array[i];
        }
      }
      // will return undefined if not found; you could return a default instead
    }

    // cuando no existen elementos a mostrar
    var noDataFound = function(error) {
      var _text = "<strong>Advertencia!</strong> No se ha obtenido ningún resutado.";
      var classWarning = "alert-warning";
      var classDanger = "alert-danger";
      var currentClass = classWarning;
      $('#alertWindow').removeClass( classWarning + " " + classDanger );
      if (error) {
        _text = "<strong>Error!</strong> Error en el servidor.";
        currentClass = classDanger;
      }

      $("#alertWindow").contents().filter( // borramos el contenido del alert nodeType 3 = Text
        function(){
          return this.nodeType == 3;
        }).remove();
      $('#alertWindow *').slice(1).remove(); // borramos todos los elementos menos el primero
      $('#alertWindow').addClass(currentClass);
      $('#alertWindow').append(
        _text
      );
      $("#alertWindow").show();
    }

  }
)();
